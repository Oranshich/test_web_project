# Test Web Project

The repository contains:
* .gitlab-ci.yml - the CI/CD configuration
* requirements.txt - contains the versions of the required Python libraries to run the Python code.
* test_web.py - Python file that contains the tests of the task.

Links to resources I used:
https://docs.pytest.org/en/stable/
https://www.dataquest.io/blog/web-scraping-beautifulsoup/
https://www.dataquest.io/blog/web-scraping-beautifulsoup/

